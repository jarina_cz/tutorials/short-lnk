import React from 'react';
import PropTypes from 'prop-types';

import LinksList from './LinksList';
import PrivateHeader from './PrivateHeader';
import AddLink from './AddLink';
import LinkListFilters from './LinkListFilters';

const Link = () => {
  return (
    <div>
      <PrivateHeader title="Your Links"/>
      <div className="page-content">
        <LinkListFilters/>
        <AddLink/>
        <LinksList/>
      </div>
    </div>
  );
};

Link.propTypes = {};
Link.defaultProps = {};

export default Link;