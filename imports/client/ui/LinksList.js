import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import {Session } from 'meteor/session';
import FlipMove from 'react-flip-move';

import { Links } from '../../api/links';
import LinkListItem from './LinkListItem';

class LinksList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      links: []
    }
  }
  componentDidMount() {
    Meteor.subscribe('links');
    this.linksTracker = Tracker.autorun(() => {
      const links = Links.find({ visible: Session.get('showVisible') }).fetch();
      this.setState({ links });
    });
  }

  componentWillUnmount() {
    this.linksTracker.stop();
  }

  renderLinksListItems() {
    if (this.state.links.length === 0) {
      return (
        <div className="item">
          <p className="item__status-message">No Links Found</p>
        </div>
      );
    }
    return this.state.links.map((link) => {
      const shortUrl = Meteor.absoluteUrl(link._id);
      return <LinkListItem key={link._id} shortUrl={shortUrl} {...link}/>;
    })
  }


  render() {
    return (
      <div>
        <div>
          <FlipMove maintainContainerHeight={true}>
            {this.renderLinksListItems()}
          </FlipMove>
        </div>
      </div>
    );
  }
}

LinksList.propTypes = {};
LinksList.defaultProps = {};

export default LinksList;
